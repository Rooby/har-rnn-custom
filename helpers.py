# TensorFlow and tf.keras
import tensorflow as tf
import functools

# Helper libraries
import numpy as np
from numpy import genfromtxt
from tensorflow.python.keras.utils import to_categorical


def get_csv_data(filepath):
    data = genfromtxt(filepath, delimiter=',')

    index = []
    x = []
    y = []
    z = []
    movement = []
    features = []

    is_first_line = True

    for line in data:
        if is_first_line == False:
            row = [line[0], line[1], line[2], line[3]]
            index.append(line[0])
            x.append(float(line[1]))
            y.append(float(line[2]))
            z.append(float(line[3]))
            features.append([row])
            movement.append(int(line[4]))
        is_first_line = False

    features = np.array(features)
    labels = to_categorical(np.array(movement), num_classes=7)
    return features, labels


def build_model(vocab_size, embedding_dim, rnn_units, batch_size):
    rnn = functools.partial(
        tf.keras.layers.GRU, recurrent_activation='sigmoid')

    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(
            vocab_size,
            embedding_dim,
            batch_input_shape=[batch_size, None]
        ),

    rnn(rnn_units,
        return_sequences=True,
        recurrent_initializer='glorot_uniform',
        stateful=True),
        tf.keras.layers.Dense(vocab_size)
    ])
    return model


def loss(labels, logits):
    return tf.keras.losses.sparse_categorical_crossentropy(labels, logits, from_logits=True)
