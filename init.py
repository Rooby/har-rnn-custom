# lstm model
import tensorflow as tf
import numpy as np

from numpy import genfromtxt
import matplotlib.pyplot as plt

from numpy import mean
from numpy import std
from pandas import read_csv
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense
from tensorflow.python.keras.layers import LSTM
from numpy import dstack
from tensorflow.python.keras.utils import to_categorical
from tensorflow.python.keras.layers import Dropout

# 0 => index = 1 => sit
# 1 => index = 2 => walk
# 2 => index = 3 => stand

# load a single file as a numpy array
def load_file(filepath):
    dataframe = read_csv(filepath, header=None, sep=",")
    return dataframe.values


# load a list of files and return as a 3d numpy array
def load_group(filenames, prefix=''):
    loaded = list()
    for name in filenames:
        data = load_file(prefix + name)
        loaded.append(data)
    # stack group so that features are the 3rd dimension
    loaded = dstack(loaded)
    return loaded


# load a dataset group, such as train or test
def load_dataset_group(group):
    filepath = './datasets/gyro_and_acc_version/'

    filenames = list()
    filenames += [group + '_acc_x.csv', group + '_acc_y.csv', group + '_acc_z.csv']
    filenames += [group + '_gyro_x.csv', group + '_gyro_y.csv', group + '_gyro_z.csv']

    x = load_group(filenames, filepath)
    y = load_file(filepath + group + '_acc_label.csv')

    return x, y

# load the dataset, returns train and test X and y elements
def load_dataset():
    trainX, trainy = load_dataset_group('train')

    x = trainX[(int)(len(trainX) * 0.2):]
    y = trainy[(int)(len(trainy) * 0.2):]

    testX = trainX[(int)(len(trainy) * 0.2 * -1):]
    testy = trainy[(int)(len(trainy) * 0.2 * -1):]

    y = y - 1
    testy = testy - 1

    y = to_categorical(y)
    testy = to_categorical(testy)
    return x, y, testX, testy

# fit and evaluate a model
def evaluate_model(trainX, trainy, testX, testy):
    verbose, epochs, batch_size = 1, 5, 64

    print("Shapes: ")
    print(trainX.shape, trainy.shape)
    n_timesteps, n_features, n_outputs = trainX.shape[1], trainX.shape[2], trainy.shape[1]

    model = Sequential()
    model.add(LSTM(
        100,
        input_shape=(n_timesteps, n_features),
    ))
    model.add(Dense(100, activation='relu'))
    model.add(Dense(n_outputs, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    tf.logging.set_verbosity(tf.logging.INFO)

    # fit network
    model.fit(trainX, trainy, epochs=epochs, batch_size=batch_size, verbose=verbose)
    # evaluate model
    _, accuracy = model.evaluate(testX, testy, verbose=verbose)

    model.save('RNN_Custom.h5')
    return accuracy, model


# summarize scores
def summarize_results(scores):
    print(scores)
    m, s = mean(scores), std(scores)
    print('Accuracy: %.3f%% (+/-%.3f)' % (m, s))


# run an experiment
def run_experiment():
    trainX, trainy, testX, testy = load_dataset()

    # repeat experiment
    scores = list()
    score, model = evaluate_model(trainX, trainy, testX, testy)
    score = score * 100.0
    scores.append(score)

    summarize_results(scores)
    print("Predict")
    predict = model.predict_classes(testX, 64)
    print(predict)


# run the experiment
run_experiment()